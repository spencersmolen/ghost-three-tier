SHELL=/bin/bash

.PHONY: compose-file
compose-file:
	source env_file \
	&& NGINX_HOST=$$NGINX_HOST \
	NGINX_PORT=$$NGINX_PORT \
	SQL_USER_PASSWORD=$$SQL_USER_PASSWORD \
	SQL_ROOT_PASSWORD=$$SQL_ROOT_PASSWORD envsubst '$$NGINX_HOST,$$NGINX_PORT,$$SQL_USER_PASSWORD,$$SQL_ROOT_PASSWORD' \
			< $(CURDIR)/docker-compose.yaml.template > ./docker-compose.yaml

passwords:
	openssl rand -base64 32 > /secrets/db_password
	openssl rand -base64 32 > /secrets/db_root_password
